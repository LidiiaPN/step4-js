
import Modal from './Modal.js';
import Card from './cardRender.js';
import modalInstance from './script.js';


let receivedId = null;

class AddComponentModal extends Modal {
  constructor(confirmCallback) {
    super(confirmCallback);
    this.form = null;
    this.modalInstance = modalInstance;
    this.cardInstance = null;
    this.receivedId = receivedId;
  }
  show() {
    this.modalInstance.show();
  }

  hide() {
    this.modalInstance.hide();
  }

  configure() {
    super.configure();

    this.title.innerText = 'Створити візит';

    this.form = document.createElement('form');
    const additionalContainer = document.createElement('div');

    const selectTemplate = document.querySelector('#select-form-template').content.cloneNode(true);
    this.form.append(selectTemplate, additionalContainer);

    let cardInstance = null;

    class Visit {
      constructor(patientName, aimVisit, descriptionVisit, urgency, statusCard) {
        this.patientName = patientName;
        this.aimVisit = aimVisit;
        this.descriptionVisit = descriptionVisit;
        this.urgency = urgency;
        this.statusCard = statusCard;
      }
    }


    class VisitDentist extends Visit {
      constructor(patientName, aimVisit, descriptionVisit, urgency, statusCard, lastVisitDate) {
        super(patientName, aimVisit, descriptionVisit, urgency, statusCard);
        this.lastVisitDate = lastVisitDate;
      }
    }


    class VisitCardiologist extends Visit {
      constructor(patientName, aimVisit, descriptionVisit, urgency, bloodPressure, age, cardioDeseases, bmi, statusCard) {
        super(patientName, aimVisit, descriptionVisit, urgency, statusCard);
        this.bloodPressure = bloodPressure;
        this.age = age;
        this.cardioDeseases = cardioDeseases;
        this.bmi = bmi;
      }
    }

    class VisitTherapist extends Visit {
      constructor(patientName, aimVisit, descriptionVisit, urgency, age, statusCard) {
        super(patientName, aimVisit, descriptionVisit, urgency, statusCard);
        this.age = age;
      }
    }


    const select = this.form.querySelector('.form-select');
    select.addEventListener('change', ({ target }) => {
      additionalContainer.innerHTML = '';
      const commonFieldsTemplate = document.querySelector('#text-form-template').content.cloneNode(true);
      additionalContainer.append(commonFieldsTemplate);

      if (target.value === 'Кардіолог') {
        const kardiologTemplate = document.querySelector('#text-form-kardiolog').content.cloneNode(true);
        additionalContainer.append(kardiologTemplate);
      } else if (target.value === 'Терапевт') {
        const terapevtTemplate = document.querySelector('#text-form-terapevt').content.cloneNode(true);
        additionalContainer.append(terapevtTemplate);
      } else if (target.value === 'Стоматолог') {
        const stomatologTemplate = document.querySelector('#text-form-stomatolog').content.cloneNode(true);
        additionalContainer.append(stomatologTemplate);
      }
    });


    this.body.append(this.form);
    this.button = document.querySelector('.js-modal-confirm-btn');
    this.button.innerText = 'Create';

    this.button.onclick = async () => {
      try {

        const select = this.form.querySelector('.form-select');
        const selectedDoctor = select.value;

        const selectStatus = this.form.querySelector('#status');
        const statusCard = selectStatus.value;

        const selectUrgency = this.form.querySelector('#urgency');
        const urgency = selectUrgency.value;

        if (!selectedDoctor) {
          console.error('Оберіть лікаря перед тим, як продовжити');
          return;
        }


        const formData = new FormData(this.form);

        const response = await axios.post('https://ajax.test-danit.com/api/v2/cards', {
          selectedDoctor,
          ...Object.fromEntries(formData),
          urgency,
          statusCard,
        }, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
          }
        });

        this.receivedId = response.data.id;
        this.hide();

        const cardInstance = new Card(Object.fromEntries(formData), selectedDoctor, this.receivedId);
        await cardInstance.renderCard();
      } catch (error) {
        console.error('Помилка при відправці запиту:', error);я
      }
    };
  }
}

export default AddComponentModal;

