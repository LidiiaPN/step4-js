

class Modal {
  constructor(confirmCallback) {
    this.confirmCallback = confirmCallback;
    this.modal = document.querySelector('#modal');
    this.title = this.modal.querySelector('.modal-title');
    this.body = this.modal.querySelector('.modal-body');
    this.button = this.modal.querySelector('.js-modal-confirm-btn');
  }

  formSerialize() {
    const inputs = this.modal.querySelectorAll('input');

    const values = {};

    inputs.forEach(input => {
      values[input.name] = input.value;
    });

    return values;
  }

  configure() {
    this.body.innerHTML = '';

    this.button.onclick = () => {
      this.confirmCallback(this)
    }
  }
}

export default Modal;

