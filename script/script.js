"use strict";

/*
email: 'snieznapanda@gmail.com',
password: 'ктки18!',
*/

//Импорт всех необходимых данных с других файлов
import LoginModal from "./loginModal.js";
import Card from "./cardRender.js";
import loginRequest from "./api/loginRequest.js";
import AddComponentModal from './AddComponentModal.js';

//поиск элементов для модального окна "логин"
const login = document.querySelector('.js-login');
const createCardBtn = document.querySelector('.js-create-card');
const logout = document.querySelector('.logout');

//поиск элементов для формы фильтрации
let searchInput = document.getElementById("search");
const searchOpen = document.getElementById("statusOpen").value;
const searchDone = document.getElementById("statusDone").value;
const filterHigh = document.getElementById("priorityHigh").value;
const filterNormal = document.getElementById("priorityNormal").value;
const filterLow = document.getElementById("priorityLow").value;

//поиск кнопок на форме фильтрации
const clearBtn = document.querySelector("#clearFilter");
const clearServer = document.querySelector("#clearServer");
const searchBtn = document.querySelector("#searchBtn");

//создача экземпляра моадльного окна
const modalInstance = new bootstrap.Modal('#modal', {
  keyboard: false
})

//проверка на авторизацию и изменение функциональных кнопок в хедере
if (localStorage.getItem('token')) {
  console.log('localStorage');
  modalInstance.hide();
  login.style.display = 'none';
  createCardBtn.style.display = 'block';
  logout.style.display = 'block';
}

//код, отвечающий за окно авторизации
login.addEventListener('click', () => {

  const modalFormFields = new LoginModal(loginModaHandler);
  modalFormFields.configure();

  const modalEditsButtons = modalFormFields.modal.querySelectorAll('.btn-warning');

  modalBody.append();

  modalEditsButtons.forEach((item) => {
    item.remove()
  })

})

//код, который вызывается в модальном окне и вызывает рендер карточек, если авторизация была успешной
const loginModaHandler = async (modal) => {
  console.log('loginModaHandler');
  const formData = modal.formSerialize();

  const data = await loginRequest(formData);
  if (data) {
    localStorage.setItem('token', data);
    modalInstance.hide();
    login.style.display = 'none';
    createCardBtn.style.display = 'block';
    logout.style.display = 'block';

    const cardContainer = document.querySelector('.cardContainer');
    cardContainer.innerHTML = '';

    try {
      const cards = await Card.fetchCardsAndRender(cardContainer);
      console.log('Загруженные карточки:', cards);
    } catch (error) {
      console.error('Ошибка при загрузке и рендеринге карточек:', error);
      cardContainer.textContent = 'Карток не знайдено.';
    }
  } else {
    alert("Неправильно введені дані")
  }

  //код, отвечающий за выход с авторизации
  logout.addEventListener('click', () => {
    localStorage.removeItem('token');
    modalInstance.hide();
    login.style.display = 'block';
    createCardBtn.style.display = 'none';
    logout.style.display = 'none';
    const cardContainer = document.querySelector('.cardContainer');
    cardContainer.innerHTML = '';

  })

  //код для создания новой карточки, вызывает модальное окно "створити візит"
  createCardBtn.addEventListener('click', () => {
    const modalFormFields = new AddComponentModal(() => { });
    modalFormFields.configure();
    modalInstance.show();

    const editButton = document.querySelector('.btn-warning');

    const title = modalFormFields.modal.querySelector('.modal-title');
    if (title && title.textContent !== 'Інформація про прийом' && editButton) {
      modalFormFields.modal.querySelector('.modal-footer').removeChild(editButton);
    }
  });
}


//код, который рендерит карточки сразу, как только будет обновление страницы после авторизации
window.addEventListener('load', async () => {
  try {
    const cardContainer = document.querySelector('.cardContainer');
    cardContainer.innerHTML = '';

    const cards = await Card.fetchCardsAndRender(cardContainer);
  } catch (error) {
    console.error('Ошибка при загрузке и рендеринге карточек:', error);
    const cardContainer = document.querySelector('.cardContainer');
    cardContainer.textContent = 'Карток не знайдено.';
  }
});

//листенер для очистки фильтров
clearBtn.addEventListener("click", () => {
  searchInput.value = '';
  const cardContainer = document.querySelector('.cardContainer');
  cardContainer.textContent = 'Карток не знайдено.';
  if (!localStorage.getItem('token')) {
    cardContainer.textContent = 'У вас немає доступу до цих функцій, будь ласка, авторизуйтесь!';
    cardContainer.style.color = "red";
    cardContainer.style.backgroundColor = "white";
    cardContainer.style.textAlign = "center";
    cardContainer.style.width = "100%";
    cardContainer.style.display = "grid";
    cardContainer.style.gridTemplateColumns = "1fr";
  } else {
    document.getElementById("statusOpen").checked = false;
    document.getElementById("statusDone").checked = false;
    document.getElementById("priorityHigh").checked = false;
    document.getElementById("priorityNormal").checked = false;
    document.getElementById("priorityLow").checked = false;
    const cards = Card.fetchCardsAndRender(cardContainer);
  }
})

//листенер для очистки всего сервера
clearServer.addEventListener('click', async () => {
  if (!localStorage.getItem('token')) {
    cardContainer.textContent = 'У вас немає доступу до цих функцій, будь ласка, авторизуйтесь!';
    cardContainer.style.color = "red";
    cardContainer.style.backgroundColor = "white";
    cardContainer.style.textAlign = "center";
    cardContainer.style.width = "100%";
    cardContainer.style.display = "grid";
    cardContainer.style.gridTemplateColumns = "1fr";
  } else {
    try {
      const response = await axios.get('https://ajax.test-danit.com/api/v2/cards', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });

      const cards = response.data;


      cards.forEach(async cardData => {
        const cardInstance = new Card(cardData);

        if (cardInstance.receivedId === '' || cardInstance.receivedId === undefined) {
          try {
            const response = await axios.delete(`https://ajax.test-danit.com/api/v2/cards/${cardData.id}`, {
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
              }
            });
            if (response.status === 200) {
              console.log('Картка успішно видалена');
            } else {
              console.error('Помилка при видаленні картки: Неправильний статус відповіді від сервера.');
            }
          } catch (error) {
            console.error('Помилка при видаленні картки:', error);
          }

        }

      });
    } catch (error) {
      console.error('Помилка при відправці запиту GET:', error);
    }
  }
});

//листенер для отслеживания изменений в инпуте поиска
searchInput.addEventListener('input', async () => {
  const cardContainer = document.querySelector('.cardContainer');
  cardContainer.innerHTML = '';
  const searchText = searchInput.value.trim().toLowerCase();
  if (!localStorage.getItem('token')) {
    cardContainer.textContent = 'У вас немає доступу до цих функцій, будь ласка, авторизуйтесь!';
    cardContainer.style.color = "red";
    cardContainer.style.backgroundColor = "white";
    cardContainer.style.textAlign = "center";
    cardContainer.style.width = "100%";
    cardContainer.style.display = "grid";
    cardContainer.style.gridTemplateColumns = "1fr";
  } else {
    try {

      const response = await axios.get('https://ajax.test-danit.com/api/v2/cards', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });

      const allCards = response.data || [];

      const filteredCards = allCards.filter(card => card.patientName && card.patientName.toLowerCase().includes(searchText));

      cardContainer.innerHTML = '';
      const cards = Card.fetchCardsAndRender(cardContainer);
    } catch (error) {
      console.error('Ошибка при фильтрации карточек:', error);
      const cardContainer = document.querySelector('.cardContainer');
      cardContainer.textContent = 'Відбулася помилка при фільтрації карток.';
    }
  }
});


export default modalInstance;

