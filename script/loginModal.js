import Modal from './Modal.js';

class LoginModal extends Modal {
  constructor(confirmCallback) {
    super(confirmCallback)
  }

  configure() {
      super.configure();
      console.log(this.title,this.body,this.button)
      this.title.innerText = 'Будь ласка, введіть свій імейл та пароль:';

      const form = document.createElement('form');
      const loginTemplate = document.querySelector('#login-form-template');
      form.append(loginTemplate.content.cloneNode(true));

      this.body.append(form);

      this.button.innerText = 'Увійти';
  }
}

export default LoginModal;
