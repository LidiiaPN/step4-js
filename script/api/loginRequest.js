

const loginRequest = async (formData) => {
  try {
    const response = await axios.post('https://ajax.test-danit.com/api/v2/cards/login', formData);
    console.log(response);
    if (response.status === 200) {
    
    return response.data;
    }
    else {
      return false;
      
      }
      
    

  }catch(err) {
    console.log(err)
  }

}

export default loginRequest
